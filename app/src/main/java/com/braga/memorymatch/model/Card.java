package com.braga.memorymatch.model;

import com.braga.memorymatch.R;

/**
 * Created by yann on 25/03/16.
 */
public class Card {

    private static int count = 0;
    private int id;
    private String name;
    private int Image;
    private boolean isSideUp;

    public Card(String name, int image) {
        this.id = ++this.count;
        this.name = name;
        Image = image;
        isSideUp = false;
    }

    public Card(int id) {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return isSideUp ? Image : R.drawable.blank;
    }

    public boolean isSideUp() {
        return isSideUp;
    }

    public void setIsSideUp(boolean isSideUp) {
        this.isSideUp = isSideUp;
    }

    public void flip(){
        this.isSideUp = !this.isSideUp;
    }
}
