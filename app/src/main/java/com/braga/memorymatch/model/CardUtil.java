package com.braga.memorymatch.model;

import android.content.Context;

import com.braga.memorymatch.R;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by yann on 25/03/16.
 */
public class CardUtil {

    private Context ctx;

    public static void ShuffleCards(ArrayList<Card> cardList){
        Collections.shuffle(cardList);
    }

    public static ArrayList<Card> generateCardList(String option) {
        // Instantiating a new list
        ArrayList<Card> cardList = new ArrayList<Card>();

        if(option.equalsIgnoreCase("programming")) {
            // Adding default values
            cardList.add(new Card("Android", R.drawable.android));
            cardList.add(new Card("Docker", R.drawable.docker));
            cardList.add(new Card("StackOverflow", R.drawable.stackoverflow));
            cardList.add(new Card("Ruby", R.drawable.ruby));
            cardList.add(new Card("RaspberryPi", R.drawable.raspberry));
            cardList.add(new Card("Arduino", R.drawable.arduino));
            cardList.add(new Card("Github", R.drawable.github));
            cardList.add(new Card("JSFiddle", R.drawable.jsfiddle));

            // Duplicating the values..
            cardList.add(new Card("Android", R.drawable.android));
            cardList.add(new Card("Docker", R.drawable.docker));
            cardList.add(new Card("StackOverflow", R.drawable.stackoverflow));
            cardList.add(new Card("Ruby", R.drawable.ruby));
            cardList.add(new Card("RaspberryPi", R.drawable.raspberry));
            cardList.add(new Card("Arduino", R.drawable.arduino));
            cardList.add(new Card("Github", R.drawable.github));
            cardList.add(new Card("JSFiddle", R.drawable.jsfiddle));
        } else {

            cardList.add(new Card("Pikachu", R.drawable.pikachu));
            cardList.add(new Card("Tepig", R.drawable.tepig));
            cardList.add(new Card("Tentacruel", R.drawable.tentacruel));
            cardList.add(new Card("Marowak", R.drawable.marowak));
            cardList.add(new Card("Charmander", R.drawable.charmander));
            cardList.add(new Card("Jynx", R.drawable.jynx));
            cardList.add(new Card("Gengar", R.drawable.gengar));
            cardList.add(new Card("Bayleaf", R.drawable.bayleaf));

            cardList.add(new Card("Pikachu", R.drawable.pikachu));
            cardList.add(new Card("Tepig", R.drawable.tepig));
            cardList.add(new Card("Tentacruel", R.drawable.tentacruel));
            cardList.add(new Card("Marowak", R.drawable.marowak));
            cardList.add(new Card("Charmander", R.drawable.charmander));
            cardList.add(new Card("Jynx", R.drawable.jynx));
            cardList.add(new Card("Gengar", R.drawable.gengar));
            cardList.add(new Card("Bayleaf", R.drawable.bayleaf));

        }
        // Shuffling the cards
        Collections.shuffle(cardList);

        return cardList;
    }
}
