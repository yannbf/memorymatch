package com.braga.memorymatch.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.braga.memorymatch.model.Card;

import java.util.List;

/**
 * Created by yann on 25/03/16.
 */
public class CardAdapter extends BaseAdapter {
    private Context context;
    private List<Card> cardList;

    public CardAdapter(Context context, List<Card> cardList) {
        this.context = context;
        this.cardList = cardList;
    }

    public void flipAllCards(){
        for(Card c : cardList){
            c.setIsSideUp(!c.isSideUp());
        }
    }

    @Override
    public int getCount() {
        return cardList.size();
    }

    @Override
    public Object getItem(int position) {
        return cardList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cardList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView iv = new ImageView(context);
        iv.setImageResource(cardList.get(position).getImage());
        iv.setAdjustViewBounds(true);
        return iv;
    }
}
