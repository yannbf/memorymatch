package com.braga.memorymatch.activity;

import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.braga.memorymatch.R;
import com.braga.memorymatch.adapter.CardAdapter;
import com.braga.memorymatch.model.Card;
import com.braga.memorymatch.model.CardUtil;

import java.util.ArrayList;

public class GameActivity extends AppCompatActivity implements GridView.OnItemClickListener {

    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private CardAdapter mCardAdapter;
    private GridView mGridView;
    private Handler mHandler;
    private ArrayList<Card> cardList;

    private TextView tvName;
    private Card nullCard = new Card(-1);
    private Card lastSelectedCard = nullCard;

    private Button btnMemory;

    private String option;
    private String playerName;

    private int gameDifficulty;
    private int flippingTime;
    private int gCountDownTime;

    private boolean isTheGameRunning;
    private CountDownTimer countDown;

    private TextView tvTimer;

    private int score = 0;
    private int numErrors = 0;

    private Toast toast;

    private void doCrazyStuff(long value){

        if((gameDifficulty == 2 || gameDifficulty == 3) && value <= 25) {
            if (value % 5 == 0) {
                CardUtil.ShuffleCards(cardList);
                updateGrid();
            }
        }
    }

    public void createTimer(long countDownTime){
        countDown = new CountDownTimer(countDownTime, 1000) {

            public void onTick(long millisUntilFinished) {
                timeleft = millisUntilFinished;
                long secondsLeft = millisUntilFinished / 1000;
                if(secondsLeft <= 30){
                    tvTimer.setTextColor(Color.rgb(255,165,0));
                }

                if(secondsLeft <= 15){
                    tvTimer.setTextColor(Color.RED);
                }

                tvTimer.setText(String.format("00:%02d", secondsLeft));
                doCrazyStuff(secondsLeft);
            }

            public void onFinish() {
                mPlayer.release();

                mPlayer = MediaPlayer.create(GameActivity.this, R.raw.gameover);
                mPlayer.start();

                if(!checkWin()){
                    tvTimer.setText(String.format("GAME OVER!"));
                    makeToast("Você perdeu...");
                    mGridView.setEnabled(false);
                    btnMemory.setText("RESTART");
                    btnMemory.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                             restart();
                        }
                    });
                }
            }
        }.start();
    }

    int length;
    long timeleft;
    @Override
    protected void onPause() {
        super.onPause();
        if(mPlayer != null) {
            mPlayer.pause();
            length = mPlayer.getCurrentPosition();
        }

        if(countDown != null) {
            countDown.cancel();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mPlayer != null) {
            mPlayer.seekTo(length);
            mPlayer.start();
        }

        if(countDown != null) {
            createTimer(timeleft);
            countDown.start();
        }
    }

    // Método para reiniciar a activity
    private void restart(){
        mPlayer.pause();

        countDown.cancel();
        countDown = null;

        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    float j = 1.0f;
    SoundPool sp = null;
    int soundId = 0;
    MediaPlayer mPlayer;

    private void doSound(){
        sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);

        //soundId = sp.load(this, R.raw.togepy, 1);

        sp.play(soundId, 1, 1, 0, 0, 1);

        int selectedMusic = 1;
        switch(gameDifficulty){
            case 1:
                selectedMusic = R.raw.background_easy;
                break;
            case 2:
                selectedMusic = R.raw.background_normal;
                break;
            case 3:
                selectedMusic = R.raw.background_hard;
                break;
        }

        mPlayer = MediaPlayer.create(this, selectedMusic);
        mPlayer.start();
    }

    private void initVariables(){
        // Recebendo dados da outra intent..
        Intent it = getIntent();
        option = it.getStringExtra("option");
        playerName = it.getStringExtra("playerName");
        gameDifficulty = it.getIntExtra("difficulty", 1);

        switch(gameDifficulty){
            case 1:
                flippingTime = 3000;
                gCountDownTime = 61000;
                break;
            case 2:
                flippingTime = 2000;
                gCountDownTime = 46000;
                break;
            case 3:
                flippingTime = 1000;
                gCountDownTime = 31000;
                break;
        }

        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvName = (TextView) findViewById(R.id.tvName);

        tvName.setText(playerName);

        mHandler = new Handler();

        // Gera-se uma lista nova
        cardList = CardUtil.generateCardList(option);

        // Seta o adapter para o gridview
        mCardAdapter = new CardAdapter(this, cardList);
        mGridView = (GridView) findViewById(R.id.gvCards);
        mGridView.setAdapter(mCardAdapter);
        mGridView.setOnItemClickListener(this);

    }

    private void initHandlers(){

        //Click do Botão
        btnMemory = (Button) findViewById(R.id.btnMemory);
        assert btnMemory != null;
        btnMemory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isTheGameRunning) {
                    startGame();
                    btnMemory.setEnabled(false);
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btnMemory.setText("CHICKEN OUT!");
                            btnMemory.setEnabled(true);
                        }
                    }, flippingTime);
                } else {
                    Intent it = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(it);
                    btnMemory.setEnabled(false);
                }
            }
        });
    }

    private void startGame(){
        isTheGameRunning = true;

        doSound();
        mCardAdapter.flipAllCards();
        updateGrid();

        // Ação que acontecerá após 1 segundo
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCardAdapter.flipAllCards();
                updateGrid();
                mGridView.setEnabled(true);
                createTimer(gCountDownTime);
            }
        }, flippingTime);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        initVariables();
        initHandlers();
        mGridView.setEnabled(false);
    }

    // Método para atualizar a grid
    public void updateGrid(){
        mCardAdapter.notifyDataSetChanged();
    }

    // Retorna num emoji :)
    public String getEmojiByUnicode(int unicode){
        return new String(Character.toChars(unicode));
    }

    public boolean checkWin(){
        if (score == 8) {
            countDown.cancel();

            mPlayer.release();

            mPlayer = MediaPlayer.create(GameActivity.this, R.raw.gameover);
            mPlayer.start();

            makeToast("Você ganhou! É sal, pivete!!" + getEmojiByUnicode(0x1F601));
            mGridView.setEnabled(false);
            btnMemory.setText("RESTART");
            btnMemory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    restart();
                }
            });
            return true;
        }

        return false;
    }

    public void toastErro(){
        String text;
        switch(numErrors){
            case 1:
                text = "Errado!";
                break;

            case 2:
            case 3:
                text = "Errou de novo..";
                break;

            case 4:
                text = "Tá de brinks, né?";
                break;

            case 5:
                text =  "Mas que burro " + getEmojiByUnicode(0x1F605);
                break;

            default:
                text =  "Desiste. Sério  " + getEmojiByUnicode(0x1F602);
                break;
        }

        makeToast(text);
    }

    public void makeToast(String text){
        if(toast != null)
            toast.cancel();
        toast  = Toast.makeText(GameActivity.this, text, Toast.LENGTH_SHORT);
        toast.show();
    }
    public boolean validateClick(Card selectedCard){
        if(selectedCard.isSideUp()){
            makeToast("Esta carta já foi virada!");
            return false;
        }

        if(selectedCard.getId() == lastSelectedCard.getId()){
            makeToast("Você selecionou a mesma carta. \nEscolha outra!");
            return false;
        }

        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mGridView.setEnabled(false);

        final Card selectedCard = cardList.get(position);

        if(selectedCard.isSideUp()){
            makeToast("Esta carta já foi virada!");
            mGridView.setEnabled(true);
            return;
        }

        if(lastSelectedCard.getId() == -1){
            lastSelectedCard = selectedCard;
            selectedCard.flip();
            updateGrid();
            mGridView.setEnabled(true);
            return;
        }

        if(validateClick(selectedCard)){
            // prevents from multiple clickings
            selectedCard.flip();
            updateGrid();

            if(selectedCard.getName().equals(lastSelectedCard.getName())){
                score++;
                lastSelectedCard = nullCard;
                mGridView.setEnabled(true);

            } else {
                numErrors++;
                toastErro();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mGridView.setEnabled(false);
                        selectedCard.flip();
                        lastSelectedCard.flip();
                        updateGrid();

                        lastSelectedCard = nullCard;
                        mGridView.setEnabled(true);
                    }
                }, 500);
            }

        } else {
            mGridView.setEnabled(true);
        }

        checkWin();
    }
}
