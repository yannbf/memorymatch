package com.braga.memorymatch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.braga.memorymatch.R;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private String option;
    private Spinner spinner;
    private TextView tvName;
    private RatingBar rbDifficulty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ArrayAdapter<CharSequence> adapter;
        Button button;
        //teste
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvName = (TextView) findViewById(R.id.etName);
        rbDifficulty = (RatingBar) findViewById(R.id.rbDifficulty);

        //Pega o spinner dos temas
        spinner = (Spinner) findViewById(R.id.options);
        //Cria o adapter com os itens do array de string no strings.xml
        adapter = ArrayAdapter.createFromResource(this,R.array.options,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Adiciona o Adapter ao Spinner
        spinner.setAdapter(adapter);
        //Adiciona o listner
        spinner.setOnItemSelectedListener(this);

        //Click do Botão
        button = (Button) findViewById(R.id.btnStart);
        assert button != null;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int difficulty = (int) rbDifficulty.getRating();
                Intent it = new Intent(MainActivity.this, GameActivity.class);
                it.putExtra("option", option);
                it.putExtra("difficulty", difficulty);
                it.putExtra("playerName", tvName.getText().toString());
                startActivity(it);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        option = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
